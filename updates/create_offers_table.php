<?php namespace Greenscreen\Website\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOffersTable extends Migration
{
    public function up()
    {
        Schema::create('greenscreen_website_offers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('icon');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('greenscreen_website_offers');
    }
}

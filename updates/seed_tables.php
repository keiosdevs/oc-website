<?php namespace Greenscreen\Website\Updates;

use Carbon\Carbon;
use Greenscreen\Website\Models\FunFact;
use Greenscreen\Website\Models\Offer;
use Greenscreen\Website\Models\Person;
use Greenscreen\Website\Models\Process;
use Greenscreen\Website\Models\Service;
use Greenscreen\Website\Models\Testimonial;
use RainLab\Blog\Models\Post;
use RainLab\Blog\Models\Category;
use October\Rain\Database\Updates\Seeder;

class SeedTables extends Seeder
{

    public function run()
    {
        FunFact::create(
            [
                'name' => 'Questions answered',
                'value' => 248
            ]);
        FunFact::create(
            [
                'name' => 'Projects finished',
                'value' => 118
            ]);
        FunFact::create(
            [
                'name' => 'Clients',
                'value' => 417
            ]);
        FunFact::create(
            [
                'name' => 'Pizzas every day',
                'value' => 333
            ]);


        Offer::create(
            [
                'name' => 'UX design',
                'description' => 'Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater',
                'icon' => 'fa-line-chart',
            ]
        );
        Offer::create(
            [
                'name' => 'UI design',
                'description' => 'Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater',
                'icon' => 'fa-cubes',
            ]
        );
        Offer::create(
            [
                'name' => 'SEO services',
                'description' => 'Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater',
                'icon' => 'fa-pie-chart',
            ]
        );
        Offer::create(
            [
                'name' => 'Something else',
                'description' => 'Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater',
                'icon' => 'fa-music',
            ]
        );

        Person::create(
            [
                'name' => 'Piotr Mechelewski',
                'position' => 'SEO',
                'description' => 'Stanowiciel',
                'facebook' => 'http://facebook.com/raziel',
                'twitter' => 'http://twitter.com/raziel',
                'linkedin' => 'http://linked.in/raziel',
                'google' => 'http://googleplus.com/raziel',
            ]
        );

        Process::create(
            [
                'position' => 1,
                'name' => 'Spotkanie',
                'icon' => 'fa-coffee',
            ]
        );

        Process::create(
            [
                'position' => 2,
                'name' => 'Scenariusz',
                'icon' => 'fa-bullhorn',
            ]
        );

        Process::create(
            [
                'position' => 3,
                'name' => 'Plan',
                'icon' => 'fa-image',
            ]
        );

        Process::create(
            [
                'position' => 4,
                'name' => 'Nagranie',
                'icon' => 'fa-film',
            ]
        );

        Process::create(
            [
                'position' => 5,
                'name' => 'Postprocessing',
                'icon' => 'fa-cogs',
            ]
        );

        Process::create(
            [
                'position' => 6,
                'name' => 'Finalizacja',
                'icon' => 'fa-space-shuttle',
            ]
        );

        Service::create(
            [
                'name' => 'Something else',
                'description' => 'Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater',
                'icon' => 'fa-music',
            ]
        );
        Service::create(
            [
                'name' => 'Something else',
                'description' => 'Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater',
                'icon' => 'fa-music',
            ]
        );
        Service::create(
            [
                'name' => 'Something else',
                'description' => 'Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater',
                'icon' => 'fa-music',
            ]
        );
        Service::create(
            [
                'name' => 'Something else',
                'description' => 'Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater',
                'icon' => 'fa-music',
            ]
        );
        Service::create(
            [
                'name' => 'Something else',
                'description' => 'Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater',
                'icon' => 'fa-music',
            ]
        );
        Service::create(
            [
                'name' => 'Something else',
                'description' => 'Backed by some of the biggest names in the industry, Firefox OS is an open platform that fosters greater',
                'icon' => 'fa-music',
            ]
        );

        Testimonial::create(
            [
                'name' => 'Stefan',
                'position' => 'Szef',
                'opinion' => 'Jest OK',
            ]
        );
        Testimonial::create(
            [
                'name' => 'Waldemar',
                'position' => 'Szef',
                'opinion' => 'Jest fajnie',
            ]
        );
        Testimonial::create(
            [
                'name' => 'Zenon',
                'position' => 'Szef',
                'opinion' => 'Jest spoko',
            ]
        );

    }

}

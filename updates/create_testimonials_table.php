<?php namespace Greenscreen\Website\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTestimonialsTable extends Migration
{
    public function up()
    {
        Schema::create('greenscreen_website_testimonials', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('position');
            $table->string('opinion');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('greenscreen_website_testimonials');
    }
}

<?php namespace Greenscreen\Website\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProcessesTable extends Migration
{
    public function up()
    {
        Schema::create('greenscreen_website_processes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('position');
            $table->string('name');
            $table->string('icon');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('greenscreen_website_processes');
    }
}

<?php namespace Greenscreen\Website\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Greenscreen\Website\Models\Offer;

/**
 * Offers Back-end Controller
 */
class Offers extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Greenscreen.Website', 'website', 'offers');
    }

    /**
     * Deleted checked offers.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $offerId) {
                if (!$offer = Offer::find($offerId)) continue;
                $offer->delete();
            }

            Flash::success(Lang::get('greenscreen.website::lang.offers.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('greenscreen.website::lang.offers.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}

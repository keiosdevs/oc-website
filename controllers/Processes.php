<?php namespace Greenscreen\Website\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Greenscreen\Website\Models\Process;

/**
 * Processes Back-end Controller
 */
class Processes extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Greenscreen.Website', 'website', 'processes');
    }

    /**
     * Deleted checked processes.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $processId) {
                if (!$process = Process::find($processId)) continue;
                $process->delete();
            }

            Flash::success(Lang::get('greenscreen.website::lang.processes.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('greenscreen.website::lang.processes.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}

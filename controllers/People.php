<?php namespace Greenscreen\Website\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Greenscreen\Website\Models\Person;

/**
 * People Back-end Controller
 */
class People extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Greenscreen.Website', 'website', 'people');
    }

    /**
     * Deleted checked people.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $personId) {
                if (!$person = Person::find($personId)) continue;
                $person->delete();
            }

            Flash::success(Lang::get('greenscreen.website::lang.people.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('greenscreen.website::lang.people.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}

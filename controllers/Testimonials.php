<?php namespace Greenscreen\Website\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Greenscreen\Website\Models\Testimonial;

/**
 * Testimonials Back-end Controller
 */
class Testimonials extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Greenscreen.Website', 'website', 'testimonials');
    }

    /**
     * Deleted checked testimonials.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $testimonialId) {
                if (!$testimonial = Testimonial::find($testimonialId)) continue;
                $testimonial->delete();
            }

            Flash::success(Lang::get('greenscreen.website::lang.testimonials.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('greenscreen.website::lang.testimonials.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}

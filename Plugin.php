<?php namespace Greenscreen\Website;

use Backend;
use System\Classes\PluginBase;

/**
 * Website Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'greenscreen.website::lang.plugin.name',
            'description' => 'greenscreen.website::lang.plugin.description',
            'author'      => 'Greenscreen',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Greenscreen\Website\Components\FunFactsComponent'     => 'funfacts',
            'Greenscreen\Website\Components\OfferComponent'        => 'offer',
            'Greenscreen\Website\Components\ProcessComponent'      => 'processes',
            'Greenscreen\Website\Components\ServicesComponent'     => 'services',
            'Greenscreen\Website\Components\TeamComponent'         => 'team',
            'Greenscreen\Website\Components\TestimonialsComponent' => 'testimonials',
            'Greenscreen\Website\Components\Helper' => 'gs_helper',
        ];
    }

    public function registerSettings()
    {
        return [
            'config' => [
                'label'       => 'Greenscreen Website',
                'icon'        => 'icon-film',
                'description' => 'Settings for GreenScreen Website',
                'class'       => 'Greenscreen\Website\Models\Settings',
                'permissions' => ['greenscreen.website.settings'],
                'order'       => 600
            ]
        ];
    }


    public function registerPermissions()
    {
        return [
            'greenscreen.website.access_posts' => [
                'tab'   => 'greenscreen.website::lang.website.tab',
                'label' => 'greenscreen.website::lang.permissions.fun_facts',
            ],
            'greenscreen.website.settings' => [
                'tab'   => 'greenscreen.website::lang.website.tab',
                'label' => 'greenscreen.website::lang.permissions.settings',
            ],
            'greenscreen.website.people'       => [
                'tab'   => 'greenscreen.website::lang.website.tab',
                'label' => 'greenscreen.website::lang.permissions.people',
            ],
            'greenscreen.website.process'      => [
                'tab'   => 'greenscreen.website::lang.website.tab',
                'label' => 'greenscreen.website::lang.permissions.process',
            ],
            'greenscreen.website.services'     => [
                'tab'   => 'greenscreen.website::lang.website.tab',
                'label' => 'greenscreen.website::lang.permissions.services',
            ],
            'greenscreen.website.offers'       => [
                'tab'   => 'greenscreen.website::lang.website.tab',
                'label' => 'greenscreen.website::lang.permissions.offers',
            ],
            'greenscreen.website.testimonials' => [
                'tab'   => 'greenscreen.website::lang.website.tab',
                'label' => 'greenscreen.website::lang.permissions.testimonials',
            ],
        ];
    }

    public function registerNavigation()
    {
        return [
            'website' => [
                'label'       => 'greenscreen.website::lang.website.menu_label',
                'url'         => Backend::url('greenscreen/website/funfacts'),
                'icon'        => 'icon-film',
                'permissions' => ['greenscreen.website.*'],
                'order'       => 30,
                'sideMenu'    => [
                    'funfacts'     => [
                        'label'       => 'greenscreen.website::lang.website.funfacts',
                        'icon'        => 'icon-star',
                        'url'         => Backend::url('greenscreen/website/funfacts'),
                        'permissions' => ['greenscreen.website.fun_facts'],
                    ],
                    'people'       => [
                        'label'       => 'greenscreen.website::lang.website.team',
                        'icon'        => 'icon-users',
                        'url'         => Backend::url('greenscreen/website/people'),
                        'permissions' => ['greenscreen.website.people'],
                    ],
                    'processes'    => [
                        'label'       => 'greenscreen.website::lang.website.process',
                        'icon'        => 'icon-list-ul',
                        'url'         => Backend::url('greenscreen/website/processes'),
                        'permissions' => ['greenscreen.website.process'],
                    ],
                    'services'     => [
                        'label'       => 'greenscreen.website::lang.website.services',
                        'icon'        => 'icon-cogs',
                        'url'         => Backend::url('greenscreen/website/services'),
                        'permissions' => ['greenscreen.website.services'],
                    ],
                    'offers'       => [
                        'label'       => 'greenscreen.website::lang.website.offer',
                        'icon'        => 'icon-money',
                        'url'         => Backend::url('greenscreen/website/offers'),
                        'permissions' => ['greenscreen.website.offers'],
                    ],
                    'testimonials' => [
                        'label'       => 'greenscreen.website::lang.website.testimonials',
                        'icon'        => 'icon-user',
                        'url'         => Backend::url('greenscreen/website/testimonials'),
                        'permissions' => ['greenscreen.website.testimonials'],
                    ],
                ],
            ],
        ];
    }


}

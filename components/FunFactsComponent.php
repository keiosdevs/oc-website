<?php namespace Greenscreen\Website\Components;

use Cms\Classes\ComponentBase;
use Greenscreen\Website\Models\FunFact;

/**
 * Class FunFactsComponent
 * @package Greenscreen\Website\Components
 */
class FunFactsComponent extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'greenscreen.website::lang.components.funfactscomponent.name',
            'description' => 'greenscreen.website::lang.components.funfactscomponent.description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     *
     */
    public function onRun()
    {
        $funFacts = FunFact::rememberForever('gs_funfacts')->get();
        $this->page['funfacts'] = $funFacts;
    }

}
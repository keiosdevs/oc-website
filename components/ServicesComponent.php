<?php namespace Greenscreen\Website\Components;

use Cms\Classes\ComponentBase;
use Greenscreen\Website\Controllers\Services;
use Greenscreen\Website\Models\Service;

class ServicesComponent extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'greenscreen.website::lang.components.servicescomponent.name',
            'description' => 'greenscreen.website::lang.components.servicescomponent.description'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $entries = Service::rememberForever('gs_services')->get();
        $this->page['services'] = $entries;
    }
}
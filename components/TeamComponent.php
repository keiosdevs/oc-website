<?php namespace Greenscreen\Website\Components;

use Cms\Classes\ComponentBase;
use Greenscreen\Website\Models\Person;

class TeamComponent extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'greenscreen.website::lang.components.teamcomponent.name',
            'description' => 'greenscreen.website::lang.components.teamcomponent.description'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $entries = Person::rememberForever('gs_people')->get();
        $this->page['team'] = $entries;
    }
}
<?php namespace Greenscreen\Website\Components;

use Cms\Classes\ComponentBase;
use Greenscreen\Website\Models\Testimonial;

class TestimonialsComponent extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'greenscreen.website::lang.components.testimonialscomponent.name',
            'description' => 'greenscreen.website::lang.components.testimonialscomponent.description'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRun()
    {
        $entries = Testimonial::rememberForever('gs_testimonials')->get();
        $this->page['testimonials'] = $entries;
    }
}
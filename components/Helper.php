<?php namespace Greenscreen\Website\Components;

use Cms\Classes\ComponentBase;
use Greenscreen\Website\Models\Settings;

class Helper extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'greenscreen.website::lang.components.helper.name',
            'description' => 'greenscreen.website::lang.components.helper.description',
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {

        $settings = Settings::instance();
        $this->page['maps_longitude'] = $settings->get('longitude');
        $this->page['maps_latitude'] = $settings->get('latitude');
        $this->page['company_address'] = $settings->get('address');
        $this->page['company_phone'] = $settings->get('phone');
        $this->page['company_email'] = $settings->get('email');
        $this->page['placeholder_image'] = $settings->get('placeholder');
        $this->page['facebook_page'] = $settings->get('facebook_page');

        if ($settings->get('enable_permissions')) {
            $about = $settings->get('about_section');
            $pricing = $settings->get('pricing_section');
            $portfolio = $settings->get('portfolio_section');
            $testimonials = $settings->get('testimonials_section');
            $offer = $settings->get('offer_section');
            $banner = $settings->get('banner_section');
            $services = $settings->get('services_section');
            $booking = $settings->get('booking_section');
            $process = $settings->get('process_section');
            $team = $settings->get('team_section');
            $details = $settings->get('details_section');
            $funfacts = $settings->get('funfacts_section');
            $blog = $settings->get('blog_section');
            $contact = $settings->get('contact_section');
        } else {
            $about = true;
            $portfolio = true;
            $pricing = false;
            $testimonials = true;
            $offer = true;
            $banner = true;
            $services = true;
            $booking = true;
            $process = true;
            $team = true;
            $details = true;
            $funfacts = true;
            $blog = true;
            $contact = true;
        }
        $this->page['about_section'] = $about;
        $this->page['portfolio_section'] = $portfolio;
        $this->page['testimonials_section'] = $testimonials;
        $this->page['offer_section'] = $offer;
        $this->page['banner_section'] = $banner;
        $this->page['services_section'] = $services;
        $this->page['booking_section'] = $booking;
        $this->page['process_section'] = $process;
        $this->page['team_section'] = $team;
        $this->page['details_section'] = $details;
        $this->page['funfacts_section'] = $funfacts;
        $this->page['blog_section'] = $blog;
        $this->page['contact_section'] = $contact;
        $this->page['pricing_section'] = $pricing;
        if($this->page->url === '/blog/:slug' || $this->page->url === '/tags/:tag' ){
            if(!$blog) {
                return \Redirect::to('/404');
            }
        }

    }


}
<?php namespace Greenscreen\Website\Components;

use Cms\Classes\ComponentBase;
use Greenscreen\Website\Models\Process;

class ProcessComponent extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'greenscreen.website::lang.components.processcomponent.name',
            'description' => 'greenscreen.website::lang.components.processcomponent.description'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $entries = Process::rememberForever('gs_processes')->get();
         $this->page['processes'] = $entries;
    }
}
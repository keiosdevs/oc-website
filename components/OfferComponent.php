<?php namespace Greenscreen\Website\Components;

use Cms\Classes\ComponentBase;
use Greenscreen\Website\Models\Offer;

class OfferComponent extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'greenscreen.website::lang.components.offercomponent.name',
            'description' => 'greenscreen.website::lang.components.offercomponent.description'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRun()
    {
        $entries = Offer::rememberForever('gs_offers')->get();
        $this->page['offer'] = $entries;
    }
}
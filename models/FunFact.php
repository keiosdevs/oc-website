<?php namespace Greenscreen\Website\Models;

use Model;

/**
 * FunFact Model
 */
class FunFact extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'greenscreen_website_fun_facts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function afterSave()
    {
        \Cache::forget('gs_funfacts');
    }

}
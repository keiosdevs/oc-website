<?php namespace Greenscreen\Website\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'greenscreen_studio';

    public $settingsFields = 'fields.yaml';
    /**
     * @var array Relations
     */
    public $attachOne = [
        'placeholder' => ['System\Models\File'],
    ];
}
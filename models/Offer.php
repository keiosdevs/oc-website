<?php namespace Greenscreen\Website\Models;

use Model;

/**
 * Offer Model
 */
class Offer extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'greenscreen_website_offers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
    public function afterSave()
    {
        \Cache::forget('gs_offers');
    }
}
<?php namespace Greenscreen\Website\Models;

use Model;

/**
 * Person Model
 */
class Person extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'greenscreen_website_people';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $attachOne = [
        'picture' => ['System\Models\File'],
    ];

    public function afterSave()
    {
        \Cache::forget('gs_people');
    }
}